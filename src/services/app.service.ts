import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
public filesSubject$ = new BehaviorSubject<File[]>([]);

}
