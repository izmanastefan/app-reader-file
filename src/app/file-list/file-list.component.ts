import { Component, OnInit } from "@angular/core";
import { FileUploadService } from "src/services/app.service";

@Component({
  selector: "app-file-list",
  templateUrl: "./file-list.component.html",
  styleUrls: ["./file-list.component.css"],
})
export class FileListComponent implements OnInit {
  constructor(private fileUploadService: FileUploadService) {}
  fileList: File[] = [];

  ngOnInit(): void {
    this.fileUploadService.filesSubject$.subscribe((data) => {
      this.fileList = data;
      console.log(data);
    });
  }

  getDateTime(file: File) {
    var myDate = new Date(file.lastModified);

    return new Intl.DateTimeFormat("de-AT", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
    })
      .format(myDate)
      .replace(/\./g, "-");
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
