import { Component } from "@angular/core";
import { FileUploadService } from "src/services/app.service";

@Component({
  selector: "app-file-upload",
  templateUrl: "./file-upload.component.html",
  styleUrls: ["./file-upload.component.css"],
})
export class FileUploadComponent {
  fileName = "";
  fileContent: any;
  fileList: File[] = [];

  constructor(private fileUploadService: FileUploadService) {}

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    let fileReader = new FileReader();

    if (file) {
      this.fileList.push(file);
      this.fileUploadService.filesSubject$.next(this.fileList);
      this.fileName = file.name;

      fileReader.onload = (e) => {
        this.fileContent = fileReader.result;
      };
      fileReader.readAsText(file);
    }
  }
}
